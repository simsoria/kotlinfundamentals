fun main(){
    val myMessage = "Happy Birthday! Justin!"
    val anotherMessage = "Happy Birthday! Justin"
//    println(myMessage)
//    println(myMessage.substring(16, 22))
    println(myMessage.compareTo(anotherMessage))
    //1
    println("$myMessage is a string which length is ${myMessage.length}")
    //2
    println(myMessage[0])
    println(myMessage[1])
    println(myMessage[2])
    println(myMessage[3])
    println(myMessage[4])
    println(myMessage[myMessage.length-1])
    //3
    val name = "Simone"
    val age = 22
    println("$name is $age years old")
    //4
    val example = "Springboot"
    val secondExample = "SPriNg booT"

    if (example == secondExample) {

        println("Strings are equal")
    }  else {

        println("Strings are not equal")
    }

    //5
    val sample = "nice day!"

    println(sample.capitalize())
    println(sample.toUpperCase())
    println(sample.toLowerCase())

    //6
    val givenName = "Simone Julienne"
    val surName = "Soria"
    val middleName = "Torres"
    val myAge = 22
    println("My full name is $givenName ${middleName.first()}. $surName")
    println("My age 10 years from now is ${myAge + 10}")

    //7
    val nickName = "sim"
    val product = 100
    val amount = 10

    println("The total amount is: ${product * amount}")
    println("${nickName.uppercase().get(0)}")

}