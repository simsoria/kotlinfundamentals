fun main(){
    //mutable
    var myName = "Simone"
    //immutable
    val mySurname = "Soria"
    println(myName)
    println(mySurname)

    val myAge = 0
    myName = "Titong"
    println(myName)
    println(myAge)
}